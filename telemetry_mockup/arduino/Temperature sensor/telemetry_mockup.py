# Copyright (c) Microsoft. All rights reserved.
# Licensed under the MIT license. See LICENSE file in the project root for full license information.

import os
import random
import time
import sys
import json
from gpiozero import CPUTemperature
# from dotenv import load_dotenv


# Using the Python Device SDK for IoT Hub:
#   https://github.com/Azure/azure-iot-sdk-python
# The sample connects to a device-specific MQTT endpoint on your IoT Hub.
from azure.iot.device import IoTHubDeviceClient, Message

# The device connection string to authenticate the device with your IoT hub.
# CONNECTION_STRING = os.environ.get('CONNECTION_STRING', str(sys.argv[1])) #"HostName=enact-hub.azure-devices.net;DeviceId=GenesisRaspberry2;SharedAccessKey=I8OYIYD/ayPB8RNOwlHZ+42CpgpgCJS6ET+HS1C0dJI="

# Define the JSON message to send to IoT Hub.
RSSI = 100
BATTERY = 100
TEMPERATURE = 20
HUMIDITY = 60
PRESSURE = 760
AX = 10.0
AY = 10.0
AZ = 10.0
LATITUDE = 59.56424
LONGITUDE = 10.42446
MSG_TXT = '{{   "rssi": {rssi}, \
                "battery": {battery}, \
                "temperature": {temperature}, \
                "humidity": {humidity}, \
                "pressure": {pressure}, \
                "ax": {ax}, \
                "ay": {ay}, \
                "az": {az}, \
                "latitude": {latitude}, \
                "longitude": {longitude}, \
                "cpu_temp": {cpu_temp}}}'

def iothub_client_init():
    # load_dotenv(verbose=True)
    # print (os.getenv("CONNECTION_STRING"))
    # Create an IoT Hub client
    conn_str = ""
    with open("/opt/gateway/app/tellu/config.credential", "rt") as f:
        data = json.load(f)
        conn_str = data['connection_string']
    client = IoTHubDeviceClient.create_from_connection_string(conn_str)
    return client

def iothub_client_telemetry_sample_run():

    try:
        client = iothub_client_init()
        print ( "TellU gateway sending periodic messages, press Ctrl-C to exit" )

        while True:            
                       
            # Measure real CPU temperature value
            cpu = CPUTemperature()
            
            # Build the message with simulated telemetry values.
            rssi = RSSI - (random.random() * 5)
            battery = BATTERY - (random.random() * 5)
            temperature = TEMPERATURE + (random.random() * 15)
            humidity = HUMIDITY + (random.random() * 20)
            pressure = PRESSURE - (random.random() * 20)
            ax = AX + (random.random() * 5)
            ay = AY + (random.random() * 5)
            az = AZ + (random.random() * 5)
            latitude = LATITUDE
            longitude = LONGITUDE
            msg_txt_formatted = MSG_TXT.format( rssi=rssi,
                                                battery = battery,
                                                temperature=temperature, 
                                                humidity=humidity, 
                                                pressure = pressure,
                                                ax = ax,
                                                ay = ay,
                                                az = az,
                                                latitude = latitude,
                                                longitude = longitude,
                                                cpu_temp = cpu.temperature)
            message = Message(msg_txt_formatted)

            # Add a custom application property to the message.
            # An IoT hub can filter on these properties without access to the message body.
            #if temperature > 30:
            #  message.custom_properties["temperatureAlert"] = "true"
            #else:
            #  message.custom_properties["temperatureAlert"] = "false"
           
            # Send the message.
            print( "Sending message: {}".format(message) )
            client.send_message(message)
            print ( "Message successfully sent" )
            time.sleep(10)

    except KeyboardInterrupt:
        print ( "IoTHubClient sample stopped" )

if __name__ == '__main__':
    print ( "ENACT: Simulated Telemetry" )
    print ( "Press Ctrl-C to exit" )
    iothub_client_telemetry_sample_run()
