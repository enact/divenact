import os
import serial
import time
import sys
import json
import requests
from dotenv import load_dotenv

# Using the Python Device SDK for IoT Hub:
#   https://github.com/Azure/azure-iot-sdk-python
# The sample connects to a device-specific MQTT endpoint on your IoT Hub.
from azure.iot.device import IoTHubDeviceClient, Message

# The device connection string to authenticate the device with your IoT hub.
# Using the Azure CLI:
# az iot hub device-identity show-connection-string --hub-name {YourIoTHubName} --device-id MyNodeDevice --output table
# CONNECTION_STRING = str(sys.argv[1]) #"HostName=enact-hub.azure-devices.net;DeviceId=arduino_one;SharedAccessKey=w6EZlH7tVBtie2NKD1JSCzyQufy6jHgMWi6ZxYbG4+Y="

# Define the JSON message to send to IoT Hub.
TEMPERATURE = 0
MSG_TXT = '{{ "temperature": {temperature} }}'

def iothub_client_init():
    load_dotenv(verbose=True)
    # print (os.getenv("CONNECTION_STRING"))
    # Create an IoT Hub client
    client = IoTHubDeviceClient.create_from_connection_string(os.getenv("CONNECTION_STRING"))
    return client

def arduino_bridge_run():

    try:
        client = iothub_client_init()
        print ( "TellU gateway sending messages from Arduino, press Ctrl-C to exit" )

        while True:            

            ser = serial
            try:
                ser = serial.Serial('/dev/ttyACM0', 9600)
                #while ser.read():
                read_serial=ser.readline().decode('utf-8').rstrip()
                print(read_serial)
                # parse json
                json_input = json.loads(read_serial)
                print(json_input)                             

                if float(read_serial) == 1000.0 :
                    print("Button pressed!")
                    # DO SOMETHING HERE!!!
                else :
                    msg_txt_formatted = MSG_TXT.format(temperature = float(read_serial))
                    message = Message(msg_txt_formatted)
                    # Send the message.
                    print( "Sending message: {}".format(message) )
                    client.send_message(message)
                    print ( "Message successfully sent" )
                time.sleep(10)
            except (serial.SerialException):
                print("Cannot read from serial port. Device disconnected? Timeout 10 seconds and trying again...")
                time.sleep(10) 

    except KeyboardInterrupt:
        print ( "Arduino bridge stopped" )

if __name__ == '__main__':
    print ( "Arduino bridge started" )
    print ( "Press Ctrl-C to exit" )
    arduino_bridge_run()