// Internal Temperature Sensor
// Example sketch for ATmega328 types.
//
// April 2012, Arduino 1.0

int ledPin = 13;  // LED connected to digital pin 13
float BUTTON_PRESSED = 1000.0; // Reserved constant for the button press event 

void setup()
{
  //pinMode(ledPin, OUTPUT);  // sets the digital pin 13 as output
  pinMode(ledPin, INPUT);   // sets the digital pin 13 as input

  Serial.begin(9600);
  //Serial.println(F("Internal Temperature Sensor"));
}

void loop()
{
  if (digitalRead(ledPin)) {
    Serial.println(1000.0, 1);
  } else {
    // Show the temperature in degrees Celsius.
    Serial.println(GetTemp(), 1);
  }
  delay(100);
}

double GetTemp(void)
{
  unsigned int wADC;
  double t;

  // The internal temperature has to be used
  // with the internal reference of 1.1V.
  // Channel 8 can not be selected with
  // the analogRead function yet.

  // Set the internal reference and mux.
  ADMUX = (_BV(REFS1) | _BV(REFS0) | _BV(MUX3));
  ADCSRA |= _BV(ADEN);  // enable the ADC

  delay(20);            // wait for voltages to become stable.

  ADCSRA |= _BV(ADSC);  // Start the ADC

  // Detect end-of-conversion
  while (bit_is_set(ADCSRA,ADSC));

  // Reading register "ADCW" takes care of how to read ADCL and ADCH.
  wADC = ADCW;

  // The offset of 324.31 could be wrong. It is just an indication.
  t = (wADC - 324.31 ) / 1.22;

  // The returned temperature is in degrees Celsius.
  return (t);
}