import serial
import time
import json
import requests
from datetime import datetime
import logging

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')

def arduino_bridge_run():

    access_token  = ""

    try:
        logging.info( "Alarm button bridge sending messages from Arduino, press Ctrl-C to exit" )

        while True:

            ser = serial
            try:
                # TODO: how to check all possible ports, not just ACM0?
                ser = serial.Serial('COM7', 9600)

                read_serial=ser.readline().decode('utf-8').rstrip()
                logging.info(read_serial)
                # parse json
                json_input = json.loads(read_serial)
                logging.info(json_input)

                if ('enrollment' in json_input):
                    logging.info("Enrollment started...")
                    headers = {'Content-Type': 'application/x-www-form-urlencoded'}

                    data = {
                        'client_id': 'mortexsa',
                        'scope': 'email openid profile security',
                        'myDeviceId': 'arduino_button',
                        'myDeviceSerialNum': '0000',
                        'device_code_validity_secondes': '300'
                    }

                    response = requests.post('https://enact-caac-auth.evidian.com/choice/oidc/devicecode', headers=headers, data=data)
                    json_response = json.loads(response.text)
                    # print(json_response['user_code'])
                    logging.info(response.text)
                    ser.write(response.text.encode())
                    ser.flush()
                    # poll the server to check if the authorisation took place
                    headers = {'Content-Type': 'application/x-www-form-urlencoded'}

                    data = {
                        'grant_type': 'urn:ietf:params:oauth:grant-type:device_code',
                        'device_code': json_response['device_code'],
                        'client_id': 'mortexsa',
                        'client_secret': 'mortexsasecret'
                    }

                    # poll for access token
                    response = requests.post('https://enact-caac-auth.evidian.com/choice/oidc/token', headers=headers, data=data)
                    while response.status_code != 200:
                        logging.info("Polling the CAAC server for access token...")
                        logging.info(response.status_code)
                        #print('sleeping:',str(datetime.now()),response.status_code)
                        #print('sleeping:',str(datetime.now()),response.headers)
                        time.sleep(1)
                        response = requests.post('https://enact-caac-auth.evidian.com/choice/oidc/token', headers=headers, data=data)

                    # store the access token on the device
                    # send confirmation to Arduino
                    with open('/home/pi/access_token.json', 'w+') as outf:
                        outf.write(response.text)
                        outf.close()

                    logging.info(response.text)
                    json_response = json.loads(response.text)
                    access_token = json_response['access_token']
                    ser.write('{"status":"200"}'.encode())
                    ser.flush()
                elif ('button' in json_input):
                    # TODO: send button event
                    logging.info("Sending button event to CAAC server...")
                    headers = {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + access_token,
                    }
                    button = "on" if json_input['button'] else "off"
                    logging.info("Button is " + button)
                    data = '{"userid":"jouve@Built-in User\'s Directory","button":"on"}' if json_input['button'] else '{"userid":"jouve@Built-in User\'s Directory","button":"off"}'
                    response = requests.post('https://enact-caac-context-input.evidian.com/contentListener', headers=headers, data=data)
                    logging.info(response.text)
                    logging.info(response.content)
                    logging.info(response.status_code)

                # time.sleep(10)
            except Exception as err:
                logging.error("Something went wrong. Exiting...")
                logging.error(err)
                exit()

    except KeyboardInterrupt:
        logging.info("Arduino bridge stopped")

if __name__ == '__main__':
    logging.info("Alarm button bridge started")
    logging.info("Press Ctrl-C to exit")
    arduino_bridge_run()