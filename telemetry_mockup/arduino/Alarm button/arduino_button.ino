#include <Thread.h>
#include <ThreadController.h>

#include <Wire.h>
#include <Adafruit_RGBLCDShield.h>
#include <utility/Adafruit_MCP23017.h>

#include <ArduinoJson.h>

// These #defines make it easy to set the backlight color
#define WHITE 0x7
#define RED 0x1
#define GREEN 0x2

#define INIT 0
#define ENROLLING 1
#define PENDING_CODE 2
#define ALARM_OFF 3
#define ALARM_ON 4
#define ERR_STATE -1

// The shield uses the I2C SCL and SDA pins. On classic Arduinos
// this is Analog 4 and 5 so you can't use those for analogRead() anymore
// However, you can connect other I2C sensors to the I2C bus and share
// the I2C bus.
Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();

// ThreadController that will controll all threads
ThreadController controller = ThreadController();

// Threads
Thread ledThread = Thread();
Thread serialInThread = Thread();
Thread timerThread = Thread();

int status = INIT;
String code = "";
int code_timer = 99;

// Create the JSON documents
DynamicJsonDocument json(256);
//DynamicJsonDocument json_upstream(256);
//DynamicJsonDocument json_downstream(256);

void setup() {
  Serial.begin(9600);
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  //lcd.setBacklight(WHITE);

  ledThread.onRun(ledCallback);
  ledThread.setInterval(100);

  serialInThread.onRun(serialInCallback);
  serialInThread.setInterval(100);

  timerThread.onRun(countdownCallback);
  timerThread.setInterval(1000);

  // Adds both threads to the controller
  controller.add(&ledThread);
  controller.add(&serialInThread);
  controller.add(&timerThread);
}

void loop() {
  // run ThreadController
  // this will check every thread inside ThreadController,
  // if it should run. If yes, he will run it;
  controller.run();

  uint8_t buttons = lcd.readButtons();
  if (buttons) {
    //Serial.println("Button pressed");
    //lcd.setCursor(0, 0);
    switch (status) {
      case (INIT):
        status = ENROLLING;
        //send request
        //DynamicJsonDocument json_upstream(128);
        json["enrollment"] = true;
        sendMessage(json);
        json.clear();
        //lcd.print(status);
        break;
      case (ENROLLING):
        //do nothing
        break;
      case (PENDING_CODE):
        //do nothing
        break;
      case (ALARM_OFF):
        //send alarm on event
        json["alarm"] = true;
        sendMessage(json);
        json.clear();
        status = ALARM_ON;
        //lcd.print(status);
        break;
      case (ALARM_ON):
        //send alarm off event
        json["alarm"] = false;
        sendMessage(json);
        json.clear();
        status = ALARM_OFF;
        //lcd.print(status);
        break;
      default:        
        break;
    }
    delay(500); //wait a bit to avoid double contacts
  }
}

// callback for ledThread
void ledCallback() {
  //lcd.clear();
  lcd.setCursor(0, 1);
  switch (status) {
    case (INIT):
      lcd.print("INIT            ");
      lcd.setBacklight(WHITE);
      break;
    case (ENROLLING):
      lcd.print("ENROLLING       ");
      lcd.setBacklight(WHITE);
      break;
    case (PENDING_CODE):
      lcd.print("PENDING_CODE    ");
      lcd.setCursor(0, 0);
      lcd.print(code);
      lcd.setBacklight(WHITE);
      break;
    case (ALARM_OFF):
      lcd.print("ALARM_OFF       ");
      lcd.setBacklight(GREEN);
      break;
    case (ALARM_ON):
      lcd.print("ALARM_ON        ");
      lcd.setBacklight(RED);
      break;
    case (ERR_STATE):
      lcd.print("ERR_STATE       ");
      lcd.setBacklight(RED);
      break;
    default:
      break;
  }
}

// callback for ledThread
void countdownCallback() {
  if (status == PENDING_CODE) {
    if (code_timer > 0) {
      lcd.setCursor(10, 0);
      //lcd.print((code_timer > 9) ? String(code_timer) : " " + String(code_timer));
      lcd.print(String(code_timer) + " sec ");
      code_timer--;
    } else {
      //reset the timer after timeout
      status = INIT;
      code_timer = 99;
      initLED();
    }
  }
}

// callback for serialThread
void serialInCallback() {

  //wait for reply
  if (Serial.available() > 0) {
    noInterrupts();
    DeserializationError err = deserializeJson(json, Serial);
    interrupts();
    if (err == DeserializationError::Ok) {
      //initLED();
      //Serial.println(json_downstream.as<String>());
      if (json.containsKey("user_code") && status == ENROLLING) {
        code = json["user_code"].as<String>();
        status = PENDING_CODE;
      } else if (json.containsKey("status") && status == PENDING_CODE) {
        status = ALARM_OFF;
        initLED();
      }
    } else {
      Serial.println(err.c_str());
      status = ERR_STATE;
    }
    json.clear();
    serialFlush();
  }
}

void sendMessage(DynamicJsonDocument json) {
  noInterrupts();
  // Send the JSON document over the serial port
  //serialFlush();
  serializeJson(json, Serial);
  Serial.println();
  //serialFlush();
  interrupts();
}

void initLED() {
  lcd.clear();
  lcd.setCursor(0, 0);
}

void serialFlush() {
  while (Serial.available() > 0) {
    char t = Serial.read();
  }
}