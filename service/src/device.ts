import { Device, Twin } from "azure-iothub";
import { Message } from "azure-iot-common";
import { shuffle } from "underscore";
import { registry, client } from "./registry";

//import { results } from "azure-iot-common";

const PROPERTY_KEYS = ['arduino', 'city', 'containerImages', 'lat', 'lon']; //current only care about "arduino" in properties.reported

/**
 * List all devices in the hub.
 */
export async function listDevices(): Promise<Device[]> {
  return new Promise<Device[]>((resolve, reject) => {
    registry.list((error: Error, deviceList: Device[]) => {
      if (error) {
        reject(error);
      } else {
        resolve(deviceList);
      }
    });
  });
}

/**
 * Returns a device twin.
 *
 * @param deviceId
 */
export async function getTwin(deviceId: string): Promise<Twin> {
  return new Promise<Twin>((resolve, reject) => {
    registry.getTwin(deviceId, (error: Error, twin: Twin) => {
      if (error) {
        reject(error);
      } else {
        resolve(twin);
      }
    });
  });
}

/**
 * Returns an existing tag.
 *
 * @param deviceId
 * @param tagName
 */
export async function getTag(
  deviceId: string,
  tagName: string
): Promise<String> {
  var twin = (await registry.getTwin(deviceId)).responseBody as Twin;
  return twin.tags[tagName];
}

/**
 * Lists existing values of a tag.
 *
 * @param tagName
 */
export async function listTagValue(tagName: string): Promise<object> {
  let deviceIds = await listEdgeIds();
  let twinPromises = [];
  for (let id of deviceIds) {
    twinPromises.push(getTwin(id));
  }
  let twins = await Promise.all(twinPromises);
  let result = {};
  for (let twin of twins) {
    let twinT = twin as Twin;
    result[twinT.deviceId] = twinT.tags[tagName];
  }
  return new Promise<object>((resolve) => {
    resolve(result);
  });
}

/**
 * Tags all devices with a tag.
 *
 * @param tagName
 * @param tagValue
 */
export async function tagTwinAll(
  tagName: string,
  tagValue: string
): Promise<string[]> {
  let deviceIds = await listEdgeIds();
  for (let id of deviceIds) {
    tagTwin(id, tagName, tagValue);
  }
  return new Promise<string[]>((resolve) => {
    // Didn't wait for the tagging to be successful... Need to be careful
    resolve(deviceIds);
  });
}

/**
 * Randomly tags a device with a tag. FIXME: why do we need this?
 *
 * @param tagName
 * @param tagValue
 * @param numberToTag
 * @param capability
 */
export async function tagTwinRandom(
  tagName: string,
  tagValue: string,
  numberToTag: number,
  capability: string = undefined
): Promise<string[]> {
  let tagedDevices = await listTagValue(tagName);
  let untagedIds: string[] = [];
  for (let id in tagedDevices) {
    if (tagedDevices[id] != tagValue) {
      untagedIds.push(id);
    }
  }

  if (capability) {
    let capabilities = await listTagValue("capability");
    untagedIds = untagedIds.filter((id) => capabilities[id] == capability);
  }

  let shuffled = <string[]>shuffle(untagedIds);
  let sliced = shuffled.slice(0, numberToTag);
  let tagged = [];
  for (let id of sliced) {
    tagged.push(tagTwin(id, tagName, tagValue));
  }

  return Promise.all(tagged);
}

/**
 * Shuffles tags among devices. FIXME: why do we need this?
 *
 * @param tagName
 * @param tagValues
 * @param deviceIds
 */
export async function tagTwinShuffled(
  tagName: string,
  tagValues: string[],
  deviceIds: string[]
): Promise<object> {
  let ids = <string[]>shuffle(deviceIds);
  let tagged: Promise<string>[] = [];
  let taggedValues: string[] = [];
  let values = [];
  var index = 0;
  for (let id of ids) {
    let tagValue = tagValues[index % tagValues.length];
    tagged.push(tagTwin(id, tagName, tagValue));
    taggedValues.push(tagValue);
    index += 1;
  }
  let taggedIds = await Promise.all(tagged);
  let result = {};
  for (var i = 0; i < taggedIds.length; i++) {
    if (taggedIds[i]) {
      result[taggedIds[i]] = taggedValues[i];
    }
  }
  return Promise.resolve(result);
}

/**
 * Tags a device twin with a tag.
 *
 * @param deviceId
 * @param tagName
 * @param tagValue
 */
export async function tagTwin(
  deviceId: string,
  tagName: string,
  tagValue: string
): Promise<string> {
  let twin = await getTwin(deviceId);
  let newTag = {};
  newTag[tagName] = tagValue;
  let twinPatch = {
    tags: newTag,
  };
  return new Promise<string>((resolve, reject) => {
    twin.update(twinPatch, (err, result) => {
      if (err) reject();
      else resolve(result.deviceId);
    });
  });
}

export async function listEdgeIds(): Promise<string[]> {
  let result: string[] = [];
  let devices = await listDevices();
  //console.log(devices)
  for (let device of devices) {
    result.push(device.deviceId);
  }
  return result;
}

/**
 * List all tags.
 */
export async function listIdTags() {
  let deviceIds = await listEdgeIds();
  let twinPromises = [];
  for (let id of deviceIds) {
    twinPromises.push(getTwin(id));
  }
  let twins = await Promise.all(twinPromises);
  let result = [];
  for (let twin of twins) {
    let twinT = twin as Twin;
    let reported = twin.properties.reported
    let toreport = {}
    for (let key of PROPERTY_KEYS){
      if (key in reported)
        toreport[`properties.${key}`] = reported[key]
    }
    result.push({
      id: twinT.deviceId,
      tags: twinT.tags,
      properties: toreport
    });
  }
  return new Promise<object>((resolve) => {
    resolve(result);
  });
}

/**
 * Invokes a direct method on a device.
 *
 * @param deviceId
 * @param methodName
 * @param payload
 */
export async function invokeDirectMethod(
  deviceId: string,
  methodName: string,
  payload: string
): Promise<any> {

  // Set the direct method name, payload, and timeout values
  let methodParams = {
    methodName: methodName,
    payload: payload,
    responseTimeoutInSeconds: 30,
  };

  // Call the direct method on the device using the defined parameters.
  let res = await client.invokeDeviceMethod(deviceId, methodParams);
  console.log(
    "Response from " + methodParams.methodName + " on " + deviceId + ":"
  );
  let result = res.result;
  console.log(result)
  console.log(JSON.stringify(result, null, 2));
  return result;

  //The code below has a problem that it always returns ""
  // client.invokeDeviceMethod(deviceId, methodParams, function (err, res) {
  //   if (err) {
  //     result =
  //       "Failed to invoke method '" +
  //       methodParams.methodName +
  //       "': " +
  //       err.message;
  //     console.error(result);
  //   } else {
  //     console.log(
  //       "Response from " + methodParams.methodName + " on " + deviceId + ":"
  //     );
  //     console.log(JSON.stringify(res, null, 2));
  //     result = res;
  //   }
  // });
  // return result;
}

/**
 * Sends a C2D message to a device.
 *
 * @param deviceId
 * @param payload
 */
export async function sendC2DMessage(
  deviceId: string,
  payload: string
): Promise<string> {
  let result = "";

  var message = new Message(payload);
  message.ack = "full";
  //message.messageId = "My Message ID";
  console.log("Sending message: " + message.getData());
  client.send(deviceId, message);

  return result;
}