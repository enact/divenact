import Router from "express";
import axios from "axios";
import { Console } from "console";

export let router = Router();

let hub_host = "127.0.0.1"


router.get('/query/type/:type', async (req, res) => {
    try{
        if(req.header("HubHost")){
            hub_host = req.header("HubHost")
            console.log(hub_host)
        }
        else{
            console.log("no host provided")
        }
        let hub_address = `http://${hub_host}:5984/public/`
        let response = await axios({baseURL: hub_address, url:`_design/main/_view/by_type?key="${req.params['type']}"`});
        let results = response.data.rows.map(x => x.value)
        console.log(JSON.stringify(results))
        results.forEach(x =>{
            let attachkeys = Object.keys(x._attachments)
            if(attachkeys)
                x["attach_url"]=`${hub_address}${x._id}/${attachkeys[0]}`
            else
                x["attach_url"]=''
        })
        res.json(results)
    }
    catch(e){
        console.log("No connection")
        res.json({"NoAccess": hub_host})
    }
})


