import Router from "express";
import {
  listDevices,
  listIdTags,
  tagTwin,
  invokeDirectMethod,
  sendC2DMessage,
} from "../device";
import { createEdgeDeploymentByEnvironment } from "../deployment";
// import { getDeployment } from "../deployment";

export let router = Router();

/**
 * Get device tags.
 */
router.get("/", async (req, res) => {
  res.json(await listIdTags());
});

/**
 *
 */
router.put("/:device", async (req, res, next) => {
  let deviceId = req.params["device"];
  let body = req.body;
  //TODO: returned result is overwritten in the loop.
  let result: Promise<string>;
  Object.keys(req.body).forEach((key) => {
    console.log(key + "---" + req.body[key]);
    result = tagTwin(deviceId, key, req.body[key]);
    if (key == "environment" && req.body[key] == "safe-mode") {
      //TODO: hard coded safe variant
      createEdgeDeploymentByEnvironment("led-safe", "safe-mode");
    }
  });
  res.json(result);
});

/**
 * Invoke direct method on a device
 */
router.put("/:device/invoke", async (req, res, next) => {
  console.log("Invoking direct method on: " + req.params["device"]);

  let deviceId = req.params["device"];
  let methodName = req.body["methodName"];
  let payload = req.body["payload"];
  let result: Promise<string>;
  result = invokeDirectMethod(deviceId, methodName, payload);
  res.json(result);
});

/**
 * Send C2D message to a device
 */
router.put("/:device/c2d", async (req, res, next) => {
  console.log("Send C2D message on server: " + req.params["device"]);
  
  let deviceId = req.params["device"];
  let payload = req.body["payload"];
  let result: Promise<string>;
  result = sendC2DMessage(deviceId, payload);
  res.json(result);
});
