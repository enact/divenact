import { Device, Twin } from "azure-iothub";
import { Message } from "azure-iot-common";
import { shuffle } from "underscore";
import { registry, client } from "./registry";
import { getTag, invokeDirectMethod, listDevices, listEdgeIds, tagTwin } from "./device";

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

export async function addRuntimeTags(){
  let deviceIDs = await listEdgeIds()
  for(let id of deviceIDs){
    console.log(id)
    
    invokeDirectMethod(id, "invokeRuntimeStatus", "")
      .then(result => {
        console.log(`from device ${id} received ${result}`)
        let payload = result.payload;
        if('arduino' in payload){
          let arduinoOn = payload['arduino']
          getTag(id, 'capability').then(tag => {
            if(tag == 'arduino'){
              if(!arduinoOn){
                console.log("I'm gonna tag arduino off")
                tagTwin(id, 'capability', 'arduino-off')
              }
            }
            else{
              if(arduinoOn){
                tagTwin(id, 'capability', 'arduino')
              }
            }
          })
        }
      })
      .catch(err =>{
        //console.log(err)
      })

  }
}

export async function loopAddRuntimeTags(){
  while(true){
    addRuntimeTags()
    await sleep(300000) //every 5 minutes
  }
}