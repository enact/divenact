import { Registry, Client } from "azure-iothub";
import * as yaml from "node-yaml";
//import { connection } from 'mongoose';

const credential = yaml.readSync("../../azureiot.credential");
export const connectionString = credential.iothub.connection;

let reg = null;
let cl = null;
console.log(connectionString)
try{
  if (connectionString != "") {
    reg = Registry.fromConnectionString(connectionString);
    cl = Client.fromConnectionString(connectionString);
  }
}
catch(error)
{
  console.log(error)
}

//console.log(reg.toString())
// reg.list()
//   .then(devices => {console.log(devices)})
//   .catch(error => {console.log(error)})


export const registry = reg;
export const client = cl;
